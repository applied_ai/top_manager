import requests
import json


def ask_gpt(
    prompt,
):
    api_key = "YOUR_API_KEY_HERE"
    endpoint = "https://api.openai.com/v1/engines/davinci-codex/completions"

    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {api_key}",
    }

    data = {
        "prompt": prompt,
        "max_tokens": 150,  # Adjust max_tokens based on your needs
    }

    try:
        response = requests.post(
            endpoint,
            headers=headers,
            json=data,
        )
        response.raise_for_status()
        result = response.json()
        return result["choices"][0]["text"].strip()
    except requests.exceptions.HTTPError as err:
        print(f"Error: {err}")
        return None


# Example usage:
user_prompt = "What is the meaning of life?"
response = ask_gpt(user_prompt)
if response:
    print(
        "ChatGPT says:",
        response,
    )
else:
    print("Failed to get a response from ChatGPT.")
